# **Reporte sobre 20 de las consultorías de software**

### Logotipo
![logotipo](imagenes/nearsoft-logo.jpg)

### Nombre
Nearsoft

### Sitio web
:globe_with_meridians: [Nearsoft](https://nearsoft.com/)

### Ubicación 
:earth_americas: https://goo.gl/maps/V3PaV8dMYXgXcafG9

### Acerca de
:books: Nearsoft tiene que ver con el desarrollo de software.
Nuestros equipos se autogestionan y entregan a tiempo.
Tratamos nuestro trabajo como un oficio y aprendemos unos de otros.
Constantemente subimos el listón.

### Servicios 
- Desarrollo de sotware

### Presencia 
- [Twitter](https://twitter.com/Nearsoft)
- [Facebook](https://www.facebook.com/Nearsoft/)
- [LinkedIn](https://www.linkedin.com/company/nearsoft/)

### Ofertas laborales 
https://nearsoft.com/join-us/

### Blog de Ingeniería 
https://nearsoft.com/blog/

### Tecnologías 
- Java 
- .NET 
- JavaScript 
- Android
- iOS 
- Python.
